package org.bitbucket.igelborstel.igelcore;

import org.bukkit.plugin.java.JavaPlugin;

public class IgelCore extends JavaPlugin 
{
	@Override
	public void onEnable()
	{
		this.getCommand("igelcoreversion").setExecutor(new VersionCommand(this));
	}
	
	@Override
	public void onDisable()
	{
	}
}

package org.bitbucket.igelborstel.igelcore;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class VersionCommand implements CommandExecutor
{

	IgelCore m;
	
	public VersionCommand(IgelCore m)
	{
		this.m = m;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("igelcoreversion"))
		{
			sender.sendMessage("[IgelCore] Version: " + m.getDescription().getVersion());
			return true;
		}
		return false;
	}
	
}

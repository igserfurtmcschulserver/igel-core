package org.bitbucket.igelborstel.igelcore.database;

import java.sql.ResultSet;

public interface IDatabase 
{
	public abstract boolean connect();
	
	public abstract boolean isConnected();
	
	public abstract void close();

	@Deprecated
	public abstract void doUpdate(String sql);

	public abstract void doUpdate(String sql, String... args);

	@Deprecated
	public abstract ResultSet doQuery(String sql);

	public abstract ResultSet doQuery(String sql, String... args);
}

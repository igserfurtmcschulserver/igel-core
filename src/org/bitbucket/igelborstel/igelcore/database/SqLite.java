package org.bitbucket.igelborstel.igelcore.database;

import java.sql.*;


public class SqLite implements IDatabase
{
	private Connection connection;
	private String pluginname;
	
	private boolean connected;
	
	public SqLite(String plugin)
	{
		this.pluginname = plugin;
	}

	@Override
	public boolean connect()
	{
		System.out.println("[IgelCore] Versuche für Plugin [" + this.pluginname + "] zur SQLite Datenbank zu verbinden...");
		try
		{
			Class.forName("org.sqlite.JDBC");
			try
			{
				this.connection = DriverManager.getConnection("jdbc:sqlite:messagelog.db");
				if(!(this.connection.isClosed()))
				{
					System.out.println("[IgelCore] Erfolgreich zur Datenbank verbunden");
					return (this.connected = true);
				}
				return (this.connected = false);
			}
			catch(SQLException ex)
			{
				System.err.println("[IgelCore] Konnte nicht zur Datenbank verbinden! Error: " + ex.getMessage());
				return (this.connected = false);
			}
		}
		catch(ClassNotFoundException ex)
		{
			System.err.println("[IgelCore] Konnte SQLite-Driver nicht finden!");
			return (this.connected = false);
		}
	}
	
	@Override
	public boolean isConnected()
	{
		return connected;
	}
	
	@Override
	public void close()
	{
		try
		{
			if(this.connection != null && (!(connection.isClosed())))
			{
				this.connection.close();
				if(connection.isClosed())
				{
					System.out.println("[IgelCore] Verbindung zur Datenbank getrennt");
				}
			}
		}
		catch(SQLException e)
		{
			System.err.println("[IgelCore] Fehler beim Schliessen der SQLite-Verbindung! Error: " +  e.getMessage());
		}
	}

	@Deprecated
	@Override
	public void doUpdate(String sql)
	{
		if(this.connected)
		{
			try
			{
				connection.createStatement().executeUpdate(sql);
			}
			catch(SQLException e)
			{
				System.err.println("[IgelCore] Konnte das Datenbank Update für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@Override
	public void doUpdate(String sql, String... args)
	{
		PreparedStatement preparedStatement;
		try
		{
			preparedStatement = connection.prepareStatement(sql);
		}
		catch(SQLException e)
		{
			System.out.println("[IgelCore] Konnte SQL Statement nicht vorbereiten!");
			e.printStackTrace();
			return;
		}
		for(int i = 0; i < args.length; ++i)
		{
			try
			{
				preparedStatement.setString(i + 1, args[i]);
			}
			catch(SQLException e)
			{
				System.err.println("[IgelCore] Konnte " + args[i] + " nicht zum SQL Statement hinzufügen!");
				e.printStackTrace();
			}
		}
		try
		{
			preparedStatement.executeUpdate();
		}
		catch(SQLException e)
		{
			System.err.println("[IgelCore] Konnte das Datenbank Update für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Deprecated
	@Override
	public ResultSet doQuery(String sql)
	{
		if(this.connected)
		{
			try
			{
				return this.connection.createStatement().executeQuery(sql);
			}
			catch(SQLException e)
			{
				System.err.println("[IgelCore] Konnte Datenbankabfrage für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}
		System.err.println("[IgelCore] Nicht zur Datenbank verbunden...");
		return null;
	}

	@Override
	public ResultSet doQuery(String sql, String... args)
	{
		ResultSet results = null;
		PreparedStatement preparedStatement;
		try
		{
			preparedStatement = connection.prepareStatement(sql);
		} catch (SQLException e)
		{
			System.out.println("[IgelCore] Konnte SQL Statement nicht vorbereiten!");
			e.printStackTrace();
			return results;
		}
		for (int i = 0; i < args.length; ++i)
		{
			try
			{
				preparedStatement.setString(i + 1, args[i]);
			} catch (SQLException e)
			{
				System.err.println("[IgelCore] Konnte " + args[i] + " nicht zum SQL Statement hinzufügen!");
				e.printStackTrace();
			}
		}
		try
		{
			results = preparedStatement.executeQuery();
			return results;
		} catch (SQLException e)
		{
			System.err.println("[IgelCore] Konnte das Datenbank Query für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
			e.printStackTrace();
			return results;
		}
	}
}



package org.bitbucket.igelborstel.igelcore.database;

import java.io.File;
import java.io.IOException;
import java.sql.*;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


public class MySql implements IDatabase
{
	private Connection connection;
	private String pluginname;
	
	private String host;
	private int port;
	private String database;
	private String user;
	private String password;
	private boolean connected;
	
	public MySql(String plugin)
	{
		this.pluginname = plugin;
		configuration();
	}
	
	private void configuration() 
	{
		File file = new File("plugins" + File.separator + this.pluginname + File.separator + "mysqldata.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		cfg.addDefault("mysql.hostname", "localhost");
		cfg.addDefault("mysql.port", Integer.valueOf(3306));
		cfg.addDefault("mysql.database", "defaultdatabase");
		cfg.addDefault("mysql.user", "root");
		cfg.addDefault("mysql.password", "toor");
		cfg.options().copyDefaults(true);
		
		try
		{
			cfg.save(file);
		}
		catch(IOException e)
		{
			System.out.println("[" + pluginname + "]" + "Konnte mysqldata.yml nicht speichern");
			e.printStackTrace();
		}
		
		this.host = cfg.getString("mysql.hostname");
		this.port = cfg.getInt("mysql.port");
		this.database = cfg.getString("mysql.database");
		this.user = cfg.getString("mysql.user");
		this.password = cfg.getString("mysql.password");
	}

	@Override
	public boolean connect()
	{
		System.out.println("[IgelCore] Versuche f�r Plugin [" + this.pluginname + "] zur Datenbank zu verbinden...");
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			try
			{
				this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
				if(!(this.connection.isClosed()))
				{
					System.out.println("[IgelCore] Erfolgreich zur Datenbank verbunden");
					return (this.connected = true);
				}
				return (this.connected = false);
			}
			catch(SQLException ex)
			{
				System.err.println("[IgelCore] Konnte nicht zur Datenbank verbinden! Error: " + ex.getMessage());
				return (this.connected = false);
			}
		}
		catch(ClassNotFoundException ex)
		{
			System.err.println("[IgelCore] Konnte MySQl-Driver nicht finden!");
			return (this.connected = false);
		}
	}
	
	@Override
	public boolean isConnected()
	{

		try
		{
			return (this.connected = connection.isValid(3));
		}
		catch (SQLException ex)
		{
			return false;
		}
	}
	
	@Override
	public void close()
	{
		try
		{
			if(this.connection != null && (!(connection.isClosed())))
			{
				this.connection.close();
				if(connection.isClosed())
				{
					System.out.println("[IgelCore] Verbindung zur Datenbank getrennt");
					connected = false;
				}
			}
		}
		catch(SQLException e)
		{
			System.err.println("[IgelCore] Fehler beim Schliessen der MySQL-Verbindung! Error: " +  e.getMessage());
		}
	}
	
	@Override
	@Deprecated
	public void doUpdate(String sql)
	{
		if(!isConnected())
		{
			connected = connect();
		}
		if(connected)
		{
			try
			{
				connection.createStatement().executeUpdate(sql);
			}
			catch(SQLException e)
			{
				System.err.println("[IgelCore] Konnte das Datenbank Update für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				System.err.println("Benutzte SQl Anweisung: " + sql);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void doUpdate(String sql, String... args)
	{
		if(!isConnected())
		{
			connected = connect();
		}
		if(connected)
		{
			PreparedStatement preparedStatement;
			try
			{
				preparedStatement = connection.prepareStatement(sql);
			} catch (SQLException e)
			{
				System.out.println("[IgelCore] Konnte SQL Statement nicht vorbereiten!");
				e.printStackTrace();
				return;
			}
			for (int i = 0; i < args.length; ++i)
			{
				try
				{
					preparedStatement.setString(i + 1, args[i]);
				} catch (SQLException e)
				{
					System.err.println("[IgelCore] Konnte " + args[i] + " nicht zum SQL Statement hinzufügen!");
					e.printStackTrace();
				}
			}
			try
			{
				preparedStatement.executeUpdate();
			} catch (SQLException e)
			{
				System.err.println("[IgelCore] Konnte das Datenbank Update für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@Deprecated
	@Override
	public ResultSet doQuery(String sql)
	{
		if (!isConnected())
		{
			connected = connect();
		}
		if (connected)
		{
			try
			{
				return this.connection.createStatement().executeQuery(sql);
			} catch (SQLException e)
			{
				System.err.println("[IgelCore] Konnte Datenbankabfrage für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}
		System.err.println("[IgelCore] Nicht zur Datenbank verbunden...");
		return null;
	}

	@Override
	public ResultSet doQuery(String sql, String... args)
	{
		if(!isConnected())
		{
			connected = connect();
		}
		if(connected)
		{
			ResultSet results = null;
			PreparedStatement preparedStatement = null;
			try
			{
				if (this.connected && connection != null)
				{
					preparedStatement = connection.prepareStatement(sql);
				}
				else
				{
					System.out.print("No connection!");
					return null;
				}
			} catch (SQLException e)
			{
				System.out.println("[IgelCore] Konnte SQL Statement nicht vorbereiten!");
				e.printStackTrace();
				return results;
			}
			for (int i = 0; i < args.length; ++i)
			{
				try
				{
					preparedStatement.setString(i + 1, args[i]);
				} catch (SQLException e)
				{
					System.err.println("[IgelCore] Konnte " + args[i] + " nicht zum SQL Statement hinzufügen!");
					e.printStackTrace();
				}
			}
			try
			{
				results = preparedStatement.executeQuery();
				return results;
			} catch (SQLException e)
			{
				System.err.println("[IgelCore] Konnte das Datenbank Query für [" + this.pluginname + "] nicht durchführen! Error: " + e.getMessage());
				e.printStackTrace();
				return results;
			}
		}
		return null;
	}
}

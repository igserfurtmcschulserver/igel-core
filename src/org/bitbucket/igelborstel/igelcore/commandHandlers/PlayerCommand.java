package org.bitbucket.igelborstel.igelcore.commandHandlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PlayerCommand implements CommandExecutor 
{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		if(!(sender instanceof Player))
		{
			sender.sendMessage("Dieses Kommando ist nur für Spieler");
			return true;
		}
		Player player = (Player) sender;
		return command(player, cmd, args);
	}

	public abstract boolean command(Player player, Command cmd, String[] args);
	
}

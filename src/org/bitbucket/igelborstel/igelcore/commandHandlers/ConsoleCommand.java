package org.bitbucket.igelborstel.igelcore.commandHandlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class ConsoleCommand implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(!(sender instanceof ConsoleCommandSender))
		{
			sender.sendMessage("Dieses Kommando lässt sich nur über die Konsole ausführen");
		}
		ConsoleCommandSender console = (ConsoleCommandSender) sender;
		return command(console, cmd, args);
	}
	
	public abstract boolean command(ConsoleCommandSender sender, Command cmd, String[] args);

}

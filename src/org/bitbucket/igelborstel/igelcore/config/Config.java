package org.bitbucket.igelborstel.igelcore.config;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class Config 
{
	private File file;
	private FileConfiguration cfg;
	private Plugin pl;
	private String filename;
	private String subdir;
	
	public Config(Plugin pl, String filename)
	{
		this.pl = pl;
		this.filename = filename;
		createConfigFile();
	}
	
	public Config(Plugin pl, String filename, String subdir)
	{
		this.pl = pl;
		this.filename = filename;
		this.subdir = subdir;
		createConfigFile();
	}
	
	private void createConfigFile()
	{
		System.out.println("[IgelCore] Erstelle zusätzliche Configdatei für " + this.pl.getDescription().getName());
		if(subdir == null || subdir.isEmpty())
		{
			file = new File("plugins" + File.separator + this.pl.getDescription().getName() + File.separator + filename + ".yml");
		}
		else
		{
			file = new File("plugins" + File.separator + this.pl.getDescription().getName() + File.separator + subdir + File.separator + filename + ".yml");
		}
		cfg = YamlConfiguration.loadConfiguration(file);
		System.out.println("[IgelCore] Config erstellt");
		saveConfig();
	}
	
	public void saveConfig()
	{
		try
		{
			cfg.save(file);
		}
		catch(IOException e)
		{
			System.err.println("[" + this.pl.getDescription().getName() + "]" + "Konnte " + this.filename + ".yml nicht speichern");
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig()
	{
		return this.cfg;
	}
	
	public void reloadConfig()
	{
		try 
		{
			cfg.load(this.file);
		} catch (IOException
				| InvalidConfigurationException e) 
		{
			System.err.println("[" + this.pl.getDescription().getName() + "]" + "Konnte " + this.filename + ".yml nicht neu laden");
			e.printStackTrace();
		}
	}

}
